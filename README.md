# Meteor



## LulzBot takes your 3D printing to another Galaxy!

Add 2.85mm BondTech LGX capability to your LulzBot 3D printer.  
The Meteor 285 Tool Head is here to bring the world of BondTech LGX to your LulzBot Mini 2, TAZ Sidekick 289, TAZ Sidekick 747, TAZ Workhorse, and TAZ Pro. 

## Find more information on our [website](https://lulzbot.com/store/lulzbot-meteor-285-tool-head-nickel-plated-brass-0-5-mm-kt-cp0189)

Source and Documentation release schedule for LulzBot Meteor 285 Tool Head:
- 08/1/2023  OHAI instructions and BOM
- 09/1/2023  .stl and harness drawings
- 10/1/2023  solidworks files


Add 1.75mm BondTech LGX capability to your LulzBot 3D printer.  
The Meteor 175 Tool Head is here to bring the world of BondTech LGX to your LulzBot Mini 2, TAZ Sidekick 289, TAZ Sidekick 747, TAZ Workhorse, and TAZ Pro. 

## Find more information on our [website](https://lulzbot.com/store/lulzbot-meteor-175-tool-head-nickel-plated-brass-0-5-mm-kt-cp0188)

Source and Documentation release schedule for LulzBot Meteor 175 Tool Head:
- 08/15/2023  OHAI instructions and BOM
- 09/15/2023  .stl and harness drawings
- 10/15/2023  solidworks files